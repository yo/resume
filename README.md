# Hi, I'm Yoginth

I'm a **Fullstack web and mobile developer** and creator of open source project called [**Taskord**](https://taskord.com) from <img src="https://www.flaticon.com/svg/static/icons/svg/3909/3909444.svg" width="17"/> India.

**📧 [Email](mailto:yoginth@icloud.com)** • **🐦 [Twitter](https://twitter.com/big1nt)** • **🔗 [LinkedIn](https://www.linkedin.com/in/yoginth)** • **🦊 [GitLab](https://gitlab.com/yo)**

### Academics

| Name | Course | CGPA | Year |
|------|--------|:----:|------|
| [**Dr. Mahalingam College of Engineering and Technology**](http://mcet.in) | B.E Computer Science and Engineering | 8.5/10 | 2016 - 2020 |

### Work Experience

| Company | Designation | Year |
|---------|-------------|------|
| [**Kāladi Consulting, Chennai**](https://kaladi.in) | Software Engineer - Web and mobile | Jan 2020 - Present |
| [**Taskord**](https://taskord.com) | Creator and Maintainer | Sep 2020 - Present |
| **Chef Labs** | Co-founder | Jan 2017 - Dec 2018 |

### Projects

| Name | Description | Techstack | Links |
|------|-------------|-----------|-------|
| [**Taskord**](https://taskord.com) | Open source social media task management system with lots of Gamifications | <img alt="PHP" src="https://img.shields.io/badge/-PHP-8892BF?style=flat-square&logo=php&logoColor=white" /> <img alt="Laravel" src="https://img.shields.io/badge/-Laravel-ff2d20?style=flat-square&logo=laravel&logoColor=white" /> <img alt="MySQL" src="https://img.shields.io/badge/-MySQL-00758F?style=flat-square&logo=mysql&logoColor=white" /> <img alt="Redis" src="https://img.shields.io/badge/-Redis-D82C20?style=flat-square&logo=redis&logoColor=white" /> <img alt="GraphQL" src="https://img.shields.io/badge/-GraphQL-E10098?style=flat-square&logo=graphql&logoColor=white" /> <img alt="git" src="https://img.shields.io/badge/-Git-F05032?style=flat-square&logo=git&logoColor=white" /> <img alt="npm" src="https://img.shields.io/badge/-NPM-CB3837?style=flat-square&logo=npm&logoColor=white" /> <img alt="html5" src="https://img.shields.io/badge/-HTML5-E34F26?style=flat-square&logo=html5&logoColor=white" /> <img alt="Sass" src="https://img.shields.io/badge/-Sass-CC6699?style=flat-square&logo=sass&logoColor=white" /> <img alt="Insomnia" src="https://img.shields.io/badge/-Insomnia-5849BE?style=flat-square&logo=insomnia&logoColor=white" /> <img alt="AWS" src="https://img.shields.io/badge/-AWS-FF9900?style=flat-square&logo=amazon&logoColor=white" /> <img alt="GitLab" src="https://img.shields.io/badge/-GitLab-FC6D27?style=flat-square&logo=gitlab&logoColor=white" /> <img alt="Memcached" src="https://img.shields.io/badge/-Memcached-565656?style=flat-square" /> <img alt="Bootstrap" src="https://img.shields.io/badge/-Bootstrap-7952b3?style=flat-square&logo=bootstrap&logoColor=white" /> <img alt="Heroku" src="https://img.shields.io/badge/-Heroku-430098?style=flat-square&logo=heroku&logoColor=white" /> <img alt="DigitalOcean" src="https://img.shields.io/badge/-DigitalOcean-008bcf?style=flat-square&logo=digitalocean&logoColor=white" /> | https://taskord.com <br> https://meow.ph/taskord <br> https://gitlab.com/taskord/taskord |
| [**Xpat**](https://apps.apple.com/in/app/xpat/id1507326139) | Expatriate Registration Mobile App during COVID-19 | <img alt="React Native" src="https://img.shields.io/badge/-React Native-45b8d8?style=flat-square&logo=react&logoColor=white" /> <img alt="Insomnia" src="https://img.shields.io/badge/-Insomnia-5849BE?style=flat-square&logo=insomnia&logoColor=white" /> <img alt="git" src="https://img.shields.io/badge/-Git-F05032?style=flat-square&logo=git&logoColor=white" /> <img alt="GitLab" src="https://img.shields.io/badge/-GitLab-FC6D27?style=flat-square&logo=gitlab&logoColor=white" /> | https://apps.apple.com/in/app/xpat/id1507326139 |
| **Kemployee** | Mobile app for employees to see latest news/blogs from the org | <img alt="React Native" src="https://img.shields.io/badge/-React Native-45b8d8?style=flat-square&logo=react&logoColor=white" /> <img alt="Insomnia" src="https://img.shields.io/badge/-Insomnia-5849BE?style=flat-square&logo=insomnia&logoColor=white" /> <img alt="git" src="https://img.shields.io/badge/-Git-F05032?style=flat-square&logo=git&logoColor=white" /> <img alt="GitLab" src="https://img.shields.io/badge/-GitLab-FC6D27?style=flat-square&logo=gitlab&logoColor=white" /> |  |
| **WWCVL Fleet** | Web app for expense booking for WWCVL employees | <img alt="Angular" src="https://img.shields.io/badge/-Angular-DD0031?style=flat-square&logo=angular&logoColor=white" /> <img alt="Insomnia" src="https://img.shields.io/badge/-Insomnia-5849BE?style=flat-square&logo=insomnia&logoColor=white" /> <img alt="git" src="https://img.shields.io/badge/-Git-F05032?style=flat-square&logo=git&logoColor=white" /> <img alt="GitLab" src="https://img.shields.io/badge/-GitLab-FC6D27?style=flat-square&logo=gitlab&logoColor=white" /> |  |

### Skills

JavaScript, PHP, Laravel, React.js, React Native, DevOps
